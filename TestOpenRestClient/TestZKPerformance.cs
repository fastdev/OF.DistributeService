﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com).

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

using OF.DistributeService.Core.Common;
using OF.DistributeService.Core.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ZooKeeperNet;
using System.Net.Sockets;
using System.Diagnostics;


namespace TestOpenRestClient
{
    public class ZookeeperServerWatcher2 : ZooKeeperSafeConnectWatcher
    {
    }

    public class TestZKPerformance
    {
        public static void TestLockAndAssembly()
        {
            object obj = new object();
            DateTime dt1 = DateTime.Now;
            for (int i1 = 0; i1 < 1000; i1++)
            {
                lock (obj)
                { 
                    
                }
            }
            DateTime dt2 = DateTime.Now;
            Util.LogInfo("time span 1:" + dt2.Subtract(dt1).TotalMilliseconds);


            dt1 = DateTime.Now;
            for (int i1 = 0; i1 < 1000; i1++)
            {
            }
            dt2 = DateTime.Now;
            Util.LogInfo("time span 2:" + dt2.Subtract(dt1).TotalMilliseconds);

            ClientConfig cc;
            dt1 = DateTime.Now;
            cc = ClientConfig.Get();
            dt2 = DateTime.Now;
            Util.LogInfo("time span 3:" + dt2.Subtract(dt1).TotalMilliseconds);

            dt1 = DateTime.Now;
            for (int i1 = 0; i1 < 10; i1++)
            {
                cc = ClientConfig.Get();
            }
            dt2 = DateTime.Now;
            Util.LogInfo("time span 4:" + dt2.Subtract(dt1).TotalMilliseconds);
        }

        public static void SafeDeletePath(ZooKeeperProxy zk, string root)
        {
            Stack<string> pathStack = new Stack<string>();
            if (zk.Exists(root, false) != null)
            {
                pathStack.Push(root);
            }
            while (pathStack.Count > 0)
            {
                string path = pathStack.Peek();
                //Util.LogInfo("now visit path:" + path);
                List<string> childList = zk.GetChildren(path, false).ToList();
                if (childList != null && childList.Count > 0)
                {
                    foreach (var child in childList)
                    {
                        pathStack.Push(path + "/" + child);
                    }
                }
                else
                {
                    path = pathStack.Pop();
                    //Util.LogInfo("delete node:" + path);
                    zk.Delete(path, -1);
                }
            }
        }

        
        public static byte[] EmptyBuffer = new byte[0];


        public static void TestTimeout()
        {

            ZooKeeperProxy zk = new ZooKeeperProxy("127.0.0.1:2181", new TimeSpan(0, 0, 0, 0, 1000), new ZookeeperServerWatcher2(), null);
            {
                Util.LogInfo(DateTime.Now.ToString("HH:mm:ss.fff"));
                try
                {
                    zk.Exists("/", false);
                }
                catch (System.Exception ex)
                {
                    Util.LogException("exists1", ex);
                }
                Thread.Sleep(5000);
                Util.LogInfo(DateTime.Now.ToString("HH:mm:ss.fff"));
                try
                {
                    zk.Exists("/", false);
                }
                catch (System.Exception ex)
                {
                    Util.LogException("exists2", ex);
                }
                Thread.Sleep(5000);
                Thread th = new Thread(new ThreadStart(() => {
                    while (true)
                    {
                        Util.LogInfo(DateTime.Now.ToString("HH:mm:ss.fff"));
                        try
                        {
                            Util.LogInfo(string.Join(",", zk.GetChildren("/", false)));
                        }
                        catch (System.Exception ex)
                        {
                            Util.LogException("exists3", ex);
                        }
                        Thread.Sleep(3000);
                    }
                }));
                th.Start();
            }
        }

        

    }
}