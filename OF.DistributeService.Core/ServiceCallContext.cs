﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Service.Temp
{
    public class ServiceCallContext
    {
        public string ServiceName
        { get; set; }

        public string MethodName
        { get; set; }

        public string Version
        { get; set; }

        public string Body
        { get; set; }
    }
}
