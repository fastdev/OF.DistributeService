﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Client.Entity
{
    public class OFDistributeServiceEndPointSearchRequest
    {
        public string ServiceAppName;
        public string Group;
        public string ServiceName;
        public string Version;
        public string Method;
    }
}
