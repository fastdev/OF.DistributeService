﻿using OF.DistributeService.Core.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Client.Entity
{
    public class ServiceVersionMethods
    {
        public string virtualPath;
        public MethodWithHttpMethod[] methodArray;
        public ServiceVersionMethods(string virtualPath, MethodWithHttpMethod[] methodArray)
        {
            this.virtualPath = virtualPath;
            this.methodArray = methodArray;
        }
    }

}
