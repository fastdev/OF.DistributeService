﻿using OF.DistributeService.Core.Common;
using OF.DistributeService.Core.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Client.Entity
{

    public class InstanceServiceVersion : Dictionary<string, ServiceVersionMethods>
    {
        private ClientConfig clientConfig = null;
        public string encodeApiBaseUrl = null;
        public string apiBaseUrl = null;
        private int checkOnlineFailedCount = 0;
        private int failedIdleCount = 0;

        public void CheckOnLine()
        {
            try
            {
                if (IsOffLine())
                {
                    if (failedIdleCount < clientConfig.MaxFailedIdleCount)
                    {
                        failedIdleCount++;
                    }
                    else
                    {
                        try
                        {
                            string okResult = getRequest();
                            checkOnlineFailedCount = 0;
                            failedIdleCount = 0;
                        }
                        catch (System.Exception ex)
                        {
                            Util.LogException("checkOnlineFailed", ex);
                            failedIdleCount = 0;
                        }
                    }
                }
                else
                {
                    try
                    {
                        string okResult = getRequest();
                        checkOnlineFailedCount = 0;
                        failedIdleCount = 0;
                    }
                    catch (System.Exception ex)
                    {
                        Util.LogException("checkOnlineFailed", ex);
                        IncFailedCount();
                    }
                }
            }
            catch (System.Exception ex2)
            {
                Util.LogException("checkOnlineFailed 2", ex2);
            }
        }

        public bool IsOffLine()
        {
            return checkOnlineFailedCount >= clientConfig.MaxCheckOnlineFailedCount;
        }

        private void IncFailedCount()
        {
            checkOnlineFailedCount++;
            if (IsOffLine())
            {
                failedIdleCount = 0;
            }
        }

        private string getRequest()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.apiBaseUrl + "/OFDistributeServiceLive/Get");
            Util.ConfigConnection(request);
            request.Method = "GET";
            request.KeepAlive = true;
            request.Timeout = clientConfig.CheckServiceOnlineTimeout;
            string retString = null;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream myResponseStream = response.GetResponseStream())
                {
                    using (StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8")))
                    {
                        retString = myStreamReader.ReadToEnd();
                    }
                }
                return retString;
            }
        }

        public InstanceServiceVersion(ClientConfig clientConfig, string encodeApiBaseUrl, string apiBaseUrl)
        {
            this.clientConfig = clientConfig;
            this.encodeApiBaseUrl = encodeApiBaseUrl;
            this.apiBaseUrl = apiBaseUrl;
        }
    }

}
