﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com).

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


using OF.DistributeService.Core.Attribute;
using OF.DistributeService.Core.Client.Entity;
using OF.DistributeService.Core.Common;
using OF.DistributeService.Core.Entity;
using OF.DistributeService.Core.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace OF.DistributeService.Core.Client
{
    public class CommonApiCallProxyProvider : ICommonApiCallProxy
    {
        private AppGroupMapInstanceList appGroupMapInstanceList;

        public CommonApiCallProxyProvider(AppGroupMapInstanceList appGroupMapInstanceList)
        {
            this.appGroupMapInstanceList = appGroupMapInstanceList;
        }

        public void InvokeVoid(CommonApiCallContext callContext, ClientToAppServiceMeta meta)
        {
            callContext.IsVoidType = true;
            Invoke<object>(callContext, meta);
        }

        public T InvokeOne<T>(CommonApiCallContext callContext, ClientToAppServiceMeta meta, OFDistributeServiceEndPointSearchResult item)
        {
            T result = default(T);
            MethodWithHttpMethod method = item.MethodInfo;
            string methodUrl = item.ServiceUrl + "/" + method.MethodName;
            if (method.HttpMethod.Equals(ApiHttpMethod.Get))
            {
                result = WebApiUtil.Get<T>(methodUrl, callContext.Params, method.ContentFormat);
            }
            else if (method.HttpMethod.Equals(ApiHttpMethod.Post))
            {
                result = WebApiUtil.Post<T>(methodUrl, callContext.Params, method.ContentFormat);
            }
            else if (method.HttpMethod.Equals(ApiHttpMethod.Put))
            {
                result = WebApiUtil.Put<T>(methodUrl, callContext.Params, method.ContentFormat);
            }
            else if (method.HttpMethod.Equals(ApiHttpMethod.Delete))
            {
                result = WebApiUtil.Delete<T>(methodUrl, callContext.Params, method.ContentFormat);
            }
            else
            {
                throw new NotSupportedException("HttpMethod " + method.HttpMethod + " is not support!");
            }
            return result;
        }

        public T Invoke<T>(CommonApiCallContext callContext, ClientToAppServiceMeta meta)
        {           
            var searchRequst = new OFDistributeServiceEndPointSearchRequest
            {
                ServiceAppName = meta.ClientToAppConfig.ServiceAppName,
                Group = meta.ClientConfig.GroupName,
                ServiceName = meta.ClientToAppServiceConfig.ServiceName,
                Version = meta.ClientToAppServiceConfig.Version,
                Method = callContext.MethodName
            };
            var item = appGroupMapInstanceList.FindEndPoint(searchRequst);
            if (item == null)
            {
                throw new NoAvailableEndPointFoundException(Util.GetJsonString(searchRequst));
            }
            T result = InvokeOne<T>(callContext, meta, item);
            return result;
        }
    }

}
