﻿using OF.DistributeService.Core.Attribute;
using OF.DistributeService.Core.Client.Entity;
using OF.DistributeService.Core.Common;
using OF.DistributeService.Core.Entity;
using OF.DistributeService.Core.Exception;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ZooKeeperNet;

namespace OF.DistributeService.Core.Client
{
    public class ServiceOnOffWatch : IWatcher
    {
        private ZooKeeperProxy zk;
        private AppGroupMapInstanceList appGroupMapInstanceList;
        public void SetZk(ZooKeeperProxy zk, AppGroupMapInstanceList appGroupMapInstanceList)
        {
            this.zk = zk;
            this.appGroupMapInstanceList = appGroupMapInstanceList;
        }

        public void Process(WatchedEvent @event)
        {
            try
            {
                if (@event.Path == null)
                {
                    return;
                }

                if (@event.Type == EventType.NodeCreated || @event.Type == EventType.NodeDeleted
                    || @event.Type == EventType.NodeChildrenChanged)
                {
                    if (appGroupMapInstanceList != null)
                    {
                        AppGroupInstanceListHolder instanceList = null;
                        if (appGroupMapInstanceList.TryGetValue(@event.Path, out instanceList))
                        {
                            appGroupMapInstanceList.FetchServiceVersionList(@event.Path);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Util.LogException("ServiceOnOffWatch", ex);
            }
        }
    }

}
