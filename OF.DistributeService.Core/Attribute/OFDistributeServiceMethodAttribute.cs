﻿/**

 * Copyright (c) 2015-2016, FastDev 刘强 (fastdev@163.com).

 *

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *      http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OF.DistributeService.Core.Attribute
{
    /// <summary>
    /// 定义方法的 HttpMethod 以及名字，此特性  必须定义在 OFDistributeServiceAttribute 定义的类或者接口上。（如果 OFDistributeServiceAttribute定义在接口上，而OFDistributeServiceMethodAttribute定义在实现类上的时候OFDistributeServiceMethodAttribute不会生效）
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class OFDistributeServiceMethodAttribute : System.Attribute
    {
        private static Type methodAttrType = typeof(OFDistributeServiceMethodAttribute);       
        private static Type objectType = typeof(object);

        private string methodName;

        /// <summary>
        /// Http Method 类型 (GET.POST.DELETE.PUT)
        /// </summary>
        public ApiHttpMethod HttpMethod
        { get; set; }

        /// <summary>
        /// Http Post内容格式 (Json/MSGPack)
        /// </summary>
        public ApiContentFormat ContentFormat 
        { get; set; }

        public string GetMethodName()
        {
            return this.methodName;
        }

        public OFDistributeServiceMethodAttribute()
        {
            HttpMethod = ApiHttpMethod.None;
            ContentFormat = ApiContentFormat.None;
        }

        public static List<OFDistributeServiceMethodAttribute> GetMethodAttribute(OFDistributeServiceAttribute serviceAttr, Type type)
        {
            MethodInfo[] methods = type.GetMethods(BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.Public);
            List<OFDistributeServiceMethodAttribute> result = new List<OFDistributeServiceMethodAttribute>(methods.Length);
            foreach (MethodInfo method in methods)
            {
                if (method.DeclaringType.FullName.Equals("System.Web.Http.ApiController") || method.DeclaringType == objectType)
                {
                    continue;
                }
                OFDistributeServiceMethodAttribute methodAttr = method.GetCustomAttribute(methodAttrType) as OFDistributeServiceMethodAttribute;
                if (methodAttr == null)
                {
                    methodAttr = new OFDistributeServiceMethodAttribute();
                }

                if (methodAttr.HttpMethod == ApiHttpMethod.None)
                {
                    methodAttr.HttpMethod = serviceAttr.DefaultMethod;
                }

                if (methodAttr.ContentFormat == ApiContentFormat.None)
                {
                    methodAttr.ContentFormat = serviceAttr.DefaultContentFormat;
                }
                methodAttr.methodName = method.Name;
                result.Add(methodAttr);
            }
            return result;
        }
    }

    public enum ApiHttpMethod : byte
    {
        None = 0,
        Get = 1,
        Post = 2,
        Put = 3,
        Delete = 4
    }

    public enum ApiContentFormat : byte
    {
        None = 0,
        JSON = 1,
        MSGPack = 2
    }
}
